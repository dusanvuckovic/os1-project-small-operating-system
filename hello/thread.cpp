#include "thread.h"

#include "core.h"
#include "pcb.h"
#include "schedule.h"
#include "system.h"
#include "callpar.h"

class CallParameters;

#define ENTER_SYSTEM_MODE_ID(id, num, par1, par2, par3) System::inSystemMode = true;\
volatile CallParameters* cp = &CallParameters(id, num, par1, par2, par3);\
volatile unsigned mySeg = FP_SEG(cp);\
volatile unsigned myOff = FP_OFF(cp);\
asm {mov ax, mySeg}; \
asm {mov bx, myOff}; \
asm int 60h

#define ENTER_SYSTEM_MODE(num, par1, par2, par3) ENTER_SYSTEM_MODE_ID(id, num, par1, par2, par3)

Thread::Thread(StackSize size, Time time) {
	lock;
	id = Core::allPCBs.getID();
	if (System::systemInitialized) {
#ifndef BCC_BLOCK_IGNORE
		ENTER_SYSTEM_MODE(0, this, &size, &time);
#endif
	} else
		Core::allPCBs.add(new PCB(this, size, time), id);
	unlock;
}

Thread::~Thread() {
	lock;
	if (System::systemInitialized) {
#ifndef BCC_BLOCK_IGNORE
		ENTER_SYSTEM_MODE(1, NULL, NULL, NULL);
#endif
	} else if (Core::allPCBs.get(id))
		delete Core::allPCBs.erase(id);
	unlock;
}

void Thread::start() {
	lock;
	if (Core::allPCBs.get(id)->state != PCB::INACTIVE) { //cannot start twice
		unlock;
		return;
	}
	Core::allPCBs.get(id)->state = PCB::READY;
	Core::allPCBs.get(id)->create_context();
	Scheduler::put(Core::allPCBs.get(id));
	unlock;
}

void Thread::run() {
}

void Thread::waitToComplete() {
	lock;
	if (System::systemInitialized) {
#ifndef BCC_BLOCK_IGNORE
		ENTER_SYSTEM_MODE(2, NULL, NULL, NULL);
#endif
	} else {
		Core::allPCBs.get(id)->waitToComplete();
		Core::dispatch(); //swapping the newly blocked thread for a new running one
	}
	unlock;
}

void dispatch() { //declared inside project text
	Core::dispatch();
}

void Thread::sleep(Time t) {
	lock;
	if (System::systemInitialized) {
#ifndef BCC_BLOCK_IGNORE
		ENTER_SYSTEM_MODE_ID(Core::running->myThread->id, 3, (void*)&t, NULL, NULL);
#endif
	} else {
		Core::running->sleep(t);
		Core::dispatch();
	}
	unlock;
}

void Thread::wrapper(Thread* running) {
	running = Core::running->myThread;
	PCB::pcb_wrapper();
	Core::dispatch(); //it cannot be selected from scheduler, but is not immediately deleted in case we need to access its data
}
