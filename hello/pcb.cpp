#include "pcb.h"
#include "core.h"
#include "schedule.h"
#include "dos.h"
#include "ivtentry.h"
#include "thread.h"

#define SET_CALL_PARAMETERS asm {mov bp, sp};\
asm {mov bx, [bp+4]};\
asm {mov ax, [bp+6]};\
asm {mov holdOFF, bx};\
asm {mov holdSEG, ax};\
cp = (CallParameters*) MK_FP(holdSEG, holdOFF)

PCB::PCB(Thread* thisThread, StackSize stacksize, Time timeSlice) : size(stacksize), tss(0), tsp(0), tbp(0),
	lockFlag(0), stack(0), myQueue() {
	state = INACTIVE;
	this->myThread = thisThread;
	this->timeSlice = timeSlice;
	this->originalTimeSlice = timeSlice;
}

PCB::~PCB()	{
	if (stack) //thread may not necessarily be started
		delete [] stack;
} 

void PCB::c_sys(CallParameters* cp) {
	static volatile unsigned holdSEG = 0, holdOFF = 0;
#ifndef BCC_BLOCK_IGNORE
	SET_CALL_PARAMETERS;
#endif
	Core::allPCBs.add(new PCB((Thread*) cp->firstArgument, *(StackSize*)cp->secondArgument, *(Time*)cp->thirdArgument), cp->id);
	System::swapNeeded = false;
	Core::endSystemCall();
}

void PCB::d_sys(CallParameters* cp) {
	static volatile unsigned holdSEG = 0, holdOFF = 0;
#ifndef BCC_BLOCK_IGNORE
	SET_CALL_PARAMETERS;
#endif
	//if (Core::allPCBs.get(cp->id))
		delete Core::allPCBs.erase(cp->id);
	System::swapNeeded = false;
	Core::endSystemCall();
}

void PCB::wtc_sys(CallParameters* cp) {
	static volatile unsigned holdSEG = 0, holdOFF = 0;
#ifndef BCC_BLOCK_IGNORE
	SET_CALL_PARAMETERS;
#endif
	Core::allPCBs.get(cp->id)->waitToComplete();
	System::swapNeeded = true;
	Core::endSystemCall();
}

void PCB::slp_sys(CallParameters* cp) {
	static volatile unsigned holdSEG = 0, holdOFF = 0;
#ifndef BCC_BLOCK_IGNORE
	SET_CALL_PARAMETERS;
#endif
	Core::allPCBs.get(cp->id)->sleep(*(Time*)(cp->firstArgument));
	System::swapNeeded = true;
	Core::endSystemCall(); //swap needed
}

void PCB::create_context()	{
	lock;
	volatile static unsigned createSP, createSS, createIP, createCS, oldSS, oldSP;
	if (size <= 0)
		{size = defaultStackSize;}
	else if (size > 65536)  //maxstacksize
		{size = MAX_STACK_SIZE;}
	stack = new unsigned[size];
	#ifndef BCC_BLOCK_IGNORE
	createSP = FP_OFF(stack + size - 1); //bottom because of 8086
	createSS = FP_SEG(stack + size - 1);
	createIP = FP_OFF(Thread::wrapper);
	createCS = FP_SEG(Thread::wrapper);
	asm{ 
		mov oldSP, sp //oldSP = sp(fun) 
		mov oldSS, ss
		// sp = createSP(thread)
		mov sp, createSP
		mov ss, createSS
		//SAVES PSW, PSWI=>1 (thread)
		pushf
		pop ax
		or ax, 0x0200
		push ax
		//SAVES PC - pcb_wrapper runs run method, starting the thread (thread)
		mov ax, createCS
		push ax
		mov ax, createIP
		push ax
		//SAVES REGISTERS (thread)
		mov ax, 0
		push ax
		push bx
		push cx
		push dx
		push es
		push ds
		push si
		push di
		push bp
		// temp function variables get new thread sp
		mov createSP, sp
		mov createSS, ss
		//old sp is back
		mov sp, oldSP
		mov ss, oldSS
	}
	#endif
	//new thread sp are saved in respective pcb variables
	this->tsp = createSP;
	this->tss = createSS;
	unlock;
}
		
void PCB::pcb_wrapper()	{
	Core::running->myThread->run();
	Core::running->state = FINISHED; //this thread has finished working
	while (!Core::running->myQueue.isEmpty()) { //threads waiting for this thread to be completed have waited enough
		PCB* temp = Core::running->myQueue.pop_front(); //when at this point, running is always ending
		temp->state = READY;
		Scheduler::put(temp); //and are now ready to be executed independently
	}
}
					
void PCB::waitToComplete() {
	if (this == Core::running || state == INACTIVE || this == Core::kernelT || state == FINISHED) {//no need to wait for running, inactive, kernel and finished threads
		return;
	} //so there can be no blocking
	Core::running->state = BLOCKEDONTHREAD; //block the waiting thread
	myQueue.push_back(Core::running); // on the running one
}

void PCB::sleep(Time t) {
	Core::running->state = PCB::SLEEPING;
	Core::timer->addToSleepQueue(Core::running, t);
}

bool PCB::decAndCheckTime() {
	if (!Core::running->originalTimeSlice) {
		return 0;
	}
	if (!(--Core::running->timeSlice)) {
		Core::running->timeSlice = Core::running->originalTimeSlice;
		return 1;
	}
	return 0;
}

unsigned* PCB::createMaxStack() {
	stack = new unsigned[MAX_STACK_SIZE];
	return stack;
}
