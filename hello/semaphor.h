#ifndef _semaphor_h_
#define _semaphor_h_

#include "core.h"

class Semaphore {
public:
	
	Semaphore (int init=1);
	virtual ~Semaphore ();

	virtual void wait ();
	virtual void signal();
		
	int val () const; // Returns the current value of the semaphore
		
protected:
	friend class KernelSem;

private:
	ID id;
};

#endif
