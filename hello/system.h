#ifndef SYSTEM_H_
#define SYSTEM_H_

#include "core.h"
#include "callpar.h"

#define numOfSysCalls (11)

#define bool unsigned int
#define true 1
#define false 0

class CallParameters;
class PCB;
class System {

private:
	unsigned* systemStack;

	static volatile unsigned sysSP, sysSS;

	static unsigned segments[numOfSysCalls];
	static unsigned offsets[numOfSysCalls];
	static void (*calls[numOfSysCalls]) (CallParameters*);

protected:
	friend class Core;
	friend class PCB;
	friend class KernelSem;
	friend class KernelEv;
	friend class Timer;

public:
	enum systemCalls {
		THRD_C,
		THRD_D,
		THRD_WTC,
		THRD_SL,
		SMPHR_C,
		SMPHR_D,
		SMPHR_W,
		SMPHR_S,
		EVNT_C,
		EVNT_D,
		EVNT_W,
	};

	static volatile bool systemInitialized;
	static volatile bool inSystemMode;
	static volatile bool swapNeeded;

	System(unsigned*);
	~System();
	static void interrupt systemCall(...);

};

#endif
