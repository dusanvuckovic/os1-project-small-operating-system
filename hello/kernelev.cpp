#include "kernelev.h"
#include "schedule.h"
#include "stdlib.h"
#include "queueg.h"
#include "callpar.h"

class QueueG;
class Core;
typedef QueueG<KernelEv> BlockedEvents;

#define SET_CALL_PARAMETERS asm {mov bp, sp};\
asm {mov bx, [bp+4]};\
asm {mov ax, [bp+6]};\
asm {mov holdOFF, bx};\
asm {mov holdSEG, ax};\
cp = (CallParameters*) MK_FP(holdSEG, holdOFF)

KernelEv* KernelEv::events[numOfIVTEntries];
IVTEntry* KernelEv::IVTTable[numOfIVTEntries];
BlockedEvents KernelEv::blockedEvents;

KernelEv::KernelEv(Event *myEv, IVTNo iN): blocked(NULL), myEvent(myEv), ivtNo(iN) {
	lock;
	creator = Core::running;
	KernelEv::events[ivtNo] = this;
	unlock;
}

KernelEv::~KernelEv() {
	lock;
	if (blocked) {
		Scheduler::put(blocked);
		--Core::noBlockedOnSemaphoresAndEvents;
	}
	KernelEv::events[ivtNo] = NULL; //cleaning up
	unlock;
}

bool KernelEv::wait() {
	lock;
	if (!blocked && Core::running == creator) {
		Core::running->state = PCB::BLOCKEDONEVENT;
		blocked = Core::running;
		++Core::noBlockedOnSemaphoresAndEvents;
		unlock;
		return true;
	}
	unlock;
	return false;
}

bool KernelEv::signal() { // always called from an interrupt routine
	if (blocked && blocked != Core::running) { //failsafe
		blocked->state = PCB::READY;
		Scheduler::put(blocked); //saving thread
		blocked = NULL; //cleaning up KernelEv
		--Core::noBlockedOnSemaphoresAndEvents;
		return true;
	}
	return false;
}

void KernelEv::c_sys(CallParameters* cp) {
	static volatile unsigned holdSEG = 0, holdOFF = 0;
#ifndef BCC_BLOCK_IGNORE
	SET_CALL_PARAMETERS;
#endif
	Core::allEvs.add(
			new KernelEv((Event*) cp->firstArgument,
					*((IVTNo*) cp->secondArgument)), cp->id);
	System::swapNeeded = false;
	Core::endSystemCall();
}

void KernelEv::d_sys(CallParameters* cp) {
	static volatile unsigned holdSEG = 0, holdOFF = 0;
#ifndef BCC_BLOCK_IGNORE
	SET_CALL_PARAMETERS;
#endif
	if (Core::allEvs.get(cp->id))
		delete Core::allEvs.erase(cp->id);
	System::swapNeeded = false;
	Core::endSystemCall();
}

void KernelEv::w_sys(CallParameters* cp) {
	static volatile unsigned holdSEG = 0, holdOFF = 0;
#ifndef BCC_BLOCK_IGNORE
	SET_CALL_PARAMETERS;
#endif
	System::swapNeeded = Core::allEvs.get(cp->id)->wait(); //no explicit dispatch
	Core::endSystemCall();
}

void KernelEv::signalInterrupt(IVTNo myNo, bool callOld) {
	lock;
	if (events[myNo])
		if (!Core::running->lockFlag)
			events[myNo]->signal();
		else
			blockedEvents.push_back(events[myNo]);
	if (callOld)
		IVTTable[myNo]->callOld();
	unlock;
}
void KernelEv::unblockNow() {
	lock;
	while (!blockedEvents.isEmpty()) {
		KernelEv *helper = blockedEvents.pop_front();
		helper->signal();
	}
	unlock;
}
