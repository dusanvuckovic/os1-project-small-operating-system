#include "callpar.h"
#include "core.h"

CallParameters::CallParameters(ID id, unsigned int opcode, void *firstArgument, void* secondArgument, void* thirdArgument) {
	this->id = id;
	this->opcode = opcode;
	this->firstArgument = firstArgument;
	this->secondArgument = secondArgument;
	this->thirdArgument = thirdArgument;
}

CallParameters::~CallParameters() {
	firstArgument = secondArgument = thirdArgument = NULL;
}
