#include "kernelsm.h"
#include "core.h"
#include "schedule.h"
#include "pcb.h"
#include "callpar.h"
#include "booleans.h"

#define SET_CALL_PARAMETERS asm {mov bp, sp};\
asm {mov bx, [bp+4]};\
asm {mov ax, [bp+6]};\
asm {mov holdOFF, bx};\
asm {mov holdSEG, ax};\
cp = (CallParameters*) MK_FP(holdSEG, holdOFF)

KernelSem::KernelSem(Semaphore* mySem, int v): val(v), myQueue(), mySemaphore(mySem) {
	lock;
	unlock;
}

KernelSem::~KernelSem()	{
	lock;
	while (!myQueue.isEmpty()) { //all previously blocked threads 'saved' by being put into the scheduler
		PCB* temp = myQueue.pop_front();
		--Core::noBlockedOnSemaphoresAndEvents;
		temp->state = PCB::READY;
		Scheduler::put(temp);
	}
	unlock;
}

bool KernelSem::wait() {
	if (--val < 0)	{
		Core::running->state = PCB::BLOCKEDONSEMAPHORE;
		++Core::noBlockedOnSemaphoresAndEvents;
		myQueue.push_back(Core::running);
		return true;
	}  
	return false;
}
	
void KernelSem::signal() {
	lock; //locked inside class Semaphore (calling)
	if (val++ < 0) {
		PCB* temp = myQueue.pop_front();
		if (temp) {
			temp->state = PCB::READY;
			--Core::noBlockedOnSemaphoresAndEvents;
			Scheduler::put(temp);
		}
	} //unlocks upon return
	unlock;
}

void KernelSem::c_sys(CallParameters* cp) {
	static volatile unsigned holdSEG = 0, holdOFF = 0;
#ifndef BCC_BLOCK_IGNORE
	SET_CALL_PARAMETERS;
#endif
	Core::allSems.add(new KernelSem((Semaphore*) cp->firstArgument, *((int*)cp->secondArgument)), cp->id);
	System::swapNeeded = false;
	Core::endSystemCall();
}

void KernelSem::d_sys(CallParameters* cp) {
	static volatile unsigned holdSEG = 0, holdOFF = 0;
#ifndef BCC_BLOCK_IGNORE
	SET_CALL_PARAMETERS;
#endif
	if (Core::allSems.get(cp->id))
		delete Core::allSems.erase(cp->id);
	System::swapNeeded = false;
	Core::endSystemCall();
}

void KernelSem::w_sys(CallParameters* cp) {
	static volatile unsigned holdSEG = 0, holdOFF = 0;
#ifndef BCC_BLOCK_IGNORE
	SET_CALL_PARAMETERS;
#endif
	System::swapNeeded = Core::allSems.get(cp->id)->wait();
	Core::endSystemCall();
}

void KernelSem::s_sys(CallParameters* cp) {
	static volatile unsigned holdSEG = 0, holdOFF = 0;
#ifndef BCC_BLOCK_IGNORE
	SET_CALL_PARAMETERS;
#endif
	Core::allSems.get(cp->id)->signal();
	System::swapNeeded = false;
	Core::endSystemCall();
}

int KernelSem::getVal() const {
	return val;
}
