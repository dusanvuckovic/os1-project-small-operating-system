#ifndef _kernelsm_h_
#define _kernelsm_h_

#include "pcb.h"
#include "core.h"
#include "queueg.h"
#include "callpar.h"

#define bool unsigned int
#define true 1
#define false 0

class PCB;
class CallParameters;

class Semaphore;
typedef QueueG<PCB> PCBQueue;
class KernelSem {

private: 

	int val;
	PCBQueue myQueue;
	Semaphore *mySemaphore;

public:

	KernelSem(Semaphore*, int);
	~KernelSem();
	bool wait();
	void signal();
	int getVal() const;

	static void c_sys(CallParameters*);
	static void d_sys(CallParameters*);
	static void w_sys(CallParameters*);
	static void s_sys(CallParameters*);
};
#endif
