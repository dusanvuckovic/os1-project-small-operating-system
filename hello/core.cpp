#include <stdlib.h>
#include "core.h"
#include "timer.h"
#include "event.h"
#include "kernelev.h"
#include "booleans.h"
#include "semaphor.h"
#include "arrayg.h"

typedef unsigned int Time;
class KernelEv;

extern void userMain(int, char**);

//INITIALIZATION
unsigned int Core::numOfEventsBlocked = 0;
unsigned int Core::noBlockedOnSemaphoresAndEvents = 0;
PCB *Core::running = NULL;
Timer* Core::timer = NULL;
PCBs Core::allPCBs(10);
Sems Core::allSems(30);
Evs Core::allEvs(10);
PCB* Core::kernelT = NULL;
PCB* Core::idleT = NULL;


void testMain() {
	Semaphore s1 (1);
	Semaphore s2 (2);
}

void Core::commence(int argc, char *argv[]) {
	//creation of kernel & elements
	new Thread(); //kernel thread; saved statically, not in scheduler
	new Idle(); //saved statically, not in scheduler; starts in constructor, minimum stack
	idleT = allPCBs.get(PCB::IDLE);
	kernelT = allPCBs.get(PCB::KERNEL);
	new System(new unsigned[MAX_STACK_SIZE]);
	timer = new Timer(); //system timer
	running = kernelT;
	userMain(argc, argv);
	//testMain();
	dispatch(); //giving up system thread for user thread, placed in scheduler in constructor
	return; //return to main
}

void Core::end() {
	lock;
	delete timer;
	delete idleT;
	delete kernelT;
	unlock;
}

void interrupt Core::dispatch() {
	//ALL REGISTERS BUT SS AND SP ARE SAVED BY THIS POINT - INTERRUPT ROUTINE
	static volatile unsigned oldSS, oldSP, oldBP;
	if (running) {
#ifndef BCC_BLOCK_IGNORE
		asm {
			mov oldSP, sp // running->tsp = SP
			mov oldSS, ss// running->tss = SS
			mov oldBP, bp// running->tbp = BP
		}
#endif

		running->tsp = oldSP; //all registers saved
		running->tss = oldSS;
		running->tbp = oldBP;

		//SCHEDULER OPERATIONS
		if ((running->state != PCB::BLOCKEDONSEMAPHORE)
				&& (running->state != PCB::BLOCKEDONEVENT)
				&& (running->state != PCB::BLOCKEDONTHREAD)
				&& (running->state != PCB::FINISHED)
				&& (running->state != PCB::SLEEPING) && (running != idleT)
				&& (running != kernelT))
			Scheduler::put(running); //hopefully not infringing upon stack, either saved inside a semaphore, event, on a thread waiting to complete or lost if thread is finished, respectively
	}
	running = Scheduler::get(); //a new thread is always needed

	//SCHEDULER TINKERING
	if (!running)
		if (!noBlockedOnSemaphoresAndEvents) //not blocked?
			running = kernelT;
		else
			running = idleT; //wait for unblock

	oldSP = running->tsp;
	oldSS = running->tss;
	oldBP = running->tbp;
#ifndef BCC_BLOCK_IGNORE
	asm { //after the change, new thread resumes where it ended previously
		mov sp, oldSP//restoring SP of current thread
		mov ss, oldSS//restoring SS of current thread
		mov bp, oldBP//restoring SS of current thread
	}
#endif
	return; //at this point all registers returned
}

void interrupt Core::endSystemCall() {
	static volatile unsigned oldSS, oldSP, oldBP;
	if (System::swapNeeded) {
		running = Scheduler::get();
		if (!running) {
			System::inSystemMode = false;
			Core::dispatch();
			return;
		}
	}
	oldSP = running->tsp;
	oldSS = running->tss;
	oldBP = running->tbp;
#ifndef BCC_BLOCK_IGNORE
	asm { //after the change, new thread resumes where it ended previously
		mov sp, oldSP//restoring SP of current thread
		mov ss, oldSS//restoring SS of current thread
		mov bp, oldBP//restoring SS of current thread
	}
#endif
	System::inSystemMode = false;
	return;
}
