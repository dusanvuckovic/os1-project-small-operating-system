#ifndef _timer_h_
#define _timer_h_

#include "thread.h"
#include "core.h"
#include <dos.h>
#include "stdlib.h"
#include "arrayg.h"
#include "queueg.h"

#define MAX_SLEEP_TIME (50)

typedef void interrupt (*pInterrupt)(...);
class ArrayG;
class QueueG;
typedef QueueG<PCB> PCBQueue;
class Timer {

private:
	static unsigned int pointerToElapsed;

public:
	
	static PCBQueue **sleepingThreads;
	static pInterrupt oldRoutine;
	Timer();
	~Timer();
	
	static void interrupt newRoutine(...);
	static void addToSleepQueue(PCB *p, Time t);
	static void wakeUp();
};

#endif
