#include "ivtentry.h"
#include "core.h"

typedef unsigned char IVTNo;
typedef void interrupt (*pInterrupt)(...);

IVTEntry::IVTEntry(IVTNo number, pInterrupt newRoutine): oldRoutine(NULL) { //swapping routines upon construction
	lock;
	KernelEv::IVTTable[number] = this;
	this->number = number;
	#ifndef BCC_BLOCK_IGNORE
	oldRoutine = getvect(number);
	setvect(number, newRoutine);
	#endif
	unlock;
}

IVTEntry::~IVTEntry() {
	lock;
	KernelEv::IVTTable[number] = NULL;
	#ifndef BCC_BLOCK_IGNORE
	setvect(number, oldRoutine);
	#endif
	unlock;
	}
	
void IVTEntry::callOld() {
	oldRoutine();
}
