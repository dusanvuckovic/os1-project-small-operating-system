#include <stdio.h>
#include <dos.h>

#include "core.h"
#include "keyevent.h"
#include "ivtentry.h"

#pragma option -w-8027
#pragma option -w-8026

int main(int argc, char *argv[]) {

	Core::commence(argc, argv);
	Core::end();
	return 0;
}
