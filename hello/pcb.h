#ifndef _pcb_h_
#define _pcb_h_

#include "thread.h"
#include "core.h"
#include "kernelev.h"
#include "system.h"
#include "callpar.h"
#include "queueg.h"

#define bool unsigned int
#define true 1
#define false 0

class Core;
class Timer;
class System;
class QueueG;
class CallParameters;
typedef QueueG<PCB> PCBQueue;
class PCB {

private:
	volatile unsigned tss;
	volatile unsigned tsp;
	volatile unsigned tbp;
	unsigned *stack;
	StackSize size;
	volatile Time timeSlice;
	volatile Time originalTimeSlice;
	PCBQueue myQueue;

				
protected:
	friend class Core;
	friend class System;

public:
	static void c_sys(CallParameters*);
	static void d_sys(CallParameters*);
	static void wtc_sys(CallParameters*);
	static void slp_sys(CallParameters*);

	PCB(Thread* thread, StackSize size, Time time);
	~PCB();
	enum MajorPCBs {
		KERNEL,
		IDLE,
	};
	enum PCBState {
		RUNNING,
		READY,
		BLOCKEDONTHREAD,
		BLOCKEDONSEMAPHORE,
		BLOCKEDONEVENT,
		INACTIVE,
		SLEEPING,
		FINISHED
	};  
	PCBState state;
	volatile unsigned int lockFlag;
	Thread *myThread;
	
	void create_context();
	static void pcb_wrapper();
	static void sleep(Time);
	void waitToComplete();

	unsigned* createMaxStack();
	static bool decAndCheckTime();
};

void dispatch();
#endif
