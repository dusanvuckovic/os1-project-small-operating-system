#ifndef CALLPAR_H_
#define CALLPAR_H_

#include "core.h"

struct CallParameters {

	ID id;
	unsigned int opcode;
	void* firstArgument;
	void* secondArgument;
	void* thirdArgument;

	CallParameters(ID, unsigned int, void*, void*, void*);
	~CallParameters();
};

#endif
