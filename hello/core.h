#ifndef _core_h_
#define _core_h_

#include <stdio.h>
#include "queueg.h"
#include "schedule.h"
#include "pcb.h"
#include <dos.h>
#include "idle.h"
#include "thread.h"
#include "timer.h"
#include "event.h"
#include "kernelev.h"
#include "kernelsm.h"
#include "ivtentry.h"
#include "user.h"
#include "arrayg.h"

#pragma warn -w8027

#define lock ++Core::running->lockFlag
#define unlock if (Core::running->lockFlag)\
	--Core::running->lockFlag

#define bool unsigned int
#define true 1
#define false 0

#define MAX_STACK_SIZE (65536)
#define numOfIVTEntries (256)

typedef unsigned int Time;
typedef unsigned char IVTNo;
typedef void interrupt (*pInterrupt)(...);

class QueueG;
class IVTEntry;
class UMain;
class ArrayG;
class PCB;
class System;
class KernelSem;
class KernelEv;
class CallParameters;
typedef ArrayG<PCB*> PCBs;
typedef ArrayG<KernelSem*> Sems;
typedef ArrayG<KernelEv*> Evs;

class Core {
public:
	static PCB *running; //currently running thread
	static Timer *timer; //system timer
	static PCBs allPCBs;
	static Sems allSems;
	static Evs allEvs;
	static PCB *kernelT, *idleT;
	static bool allDone; //a static internal indicator
	static unsigned int noBlockedOnSemaphoresAndEvents; //to be used inside dispatch determining whether a total system exit is needed
	static unsigned int numOfEventsBlocked;

	static void commence(int, char**); //begin
	static void end(); //end
	static void interrupt dispatch();
	static void interrupt endSystemCall();
};
#endif
