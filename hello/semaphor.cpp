#include "semaphor.h"
#include "kernelsm.h"

class KernelSem;

#define ENTER_SYSTEM_MODE(num, par1, par2, par3) System::inSystemMode = true;\
volatile CallParameters* cp = &CallParameters(id, num, par1, par2, par3);\
volatile unsigned mySeg = FP_SEG(cp);\
volatile unsigned myOff = FP_OFF(cp);\
asm {mov ax, mySeg}; \
asm {mov bx, myOff}; \
asm int 60h

Semaphore::Semaphore(int init) {
	lock;
	id = Core::allSems.getID();
	if (System::systemInitialized) {
#ifndef BCC_BLOCK_IGNORE
		ENTER_SYSTEM_MODE(4, (void*)this, &init, NULL);
#endif
	} else
		Core::allSems.add(new KernelSem(this, init), id);
	unlock;
}

Semaphore::~Semaphore() {
	lock;
	if (System::systemInitialized) {
#ifndef BCC_BLOCK_IGNORE
		ENTER_SYSTEM_MODE(5, NULL, NULL, NULL);
#endif
	} else if (Core::allSems.get(id))
		delete Core::allSems.erase(id);
	unlock;
}

void Semaphore::wait() {
	lock;
	if (System::systemInitialized) {
#ifndef BCC_BLOCK_IGNORE
		ENTER_SYSTEM_MODE(6, NULL, NULL, NULL);
#endif
	} else {
		bool shouldDispatch = Core::allSems.get(id)->wait();
		if (shouldDispatch) {
			unlock;
			Core::dispatch();
		}
	}
	unlock;
}

void Semaphore::signal() {
	lock;
	if (System::systemInitialized) {
#ifndef BCC_BLOCK_IGNORE
		ENTER_SYSTEM_MODE(7, NULL, NULL, NULL);
#endif
	} else
		Core::allSems.get(id)->signal();
	unlock;
}

int Semaphore::val() const {
	return Core::allSems.get(id)->getVal();
}
