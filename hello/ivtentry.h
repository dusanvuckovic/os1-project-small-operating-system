#ifndef _ivtentry_h_
#define _ivtentry_h_

#include <dos.h>
#include "core.h"
#include "event.h"
#include "timer.h"
#include "kernelsm.h"
#include "queueg.h"
#include "kernelev.h"

class Event;
class QueueG;
class KernelEv;
#define numOfIVTEntries (256)

#define PREPAREENTRY(num, toCall)\
	class Core;\
	class IVTEntry;\
	typedef unsigned char IVTNo;\
	typedef void interrupt (*pInterrupt)(...);\
	void interrupt eventinstance##num(...) {\
		KernelEv::signalInterrupt(num, toCall);\
	}\
	IVTEntry ivtinstance##no(num, eventinstance##num);
	
typedef unsigned char IVTNo;
typedef void interrupt (*pInterrupt)(...);

	
class IVTEntry {

public:
	IVTEntry(IVTNo, pInterrupt);
	~IVTEntry();
	void callOld();

private:
	IVTNo number; //of the corresponding event
	pInterrupt oldRoutine; //taken from
};

#endif
