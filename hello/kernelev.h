#ifndef _kernelev_h_
#define _kernelev_h_

#include <dos.h>
#include "event.h"
#include "pcb.h"
#include "core.h"
#include "thread.h"
#include "ivtentry.h"
#include "callpar.h"
#include "queueg.h"
#include "booleans.h"

#define numOfIVTEntries (256)

typedef unsigned char IVTNo;
typedef void interrupt (*pInterrupt)(...);

class Event;
class PCB;
class Core;
class CallParameters;
class QueueG;

class KernelEv {

	typedef QueueG<KernelEv> BlockedEvents;

private:
	friend class Core;
	friend class IVTEntry;
	friend class Timer;

	IVTNo ivtNo;
	PCB *creator; //of the event
	PCB *blocked; //upon the event
	Event *myEvent;

	static KernelEv* events[numOfIVTEntries];
	static IVTEntry* IVTTable[numOfIVTEntries];
	static BlockedEvents blockedEvents;

public:

	KernelEv(Event*, IVTNo);
	~KernelEv();

	static void signalInterrupt(IVTNo, bool);
	static void unblockNow();

	bool wait();
	bool signal();

	static void c_sys(CallParameters*);
	static void d_sys(CallParameters*);
	static void w_sys(CallParameters*);
};

#endif
