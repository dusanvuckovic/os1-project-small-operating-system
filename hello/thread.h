#ifndef _thread_h_
#define _thread_h_

typedef unsigned long StackSize;
const StackSize defaultStackSize = 4096;
typedef unsigned int Time;
const Time defaultTimeSlice = 2;
typedef int ID;

class PCB;

class Thread {
public:

	void start();
	void waitToComplete();
	virtual ~Thread();
	static void sleep(Time timeToSleep);
			
protected:
	friend class PCB;
	friend class Core;
	friend class Idle;
	Thread (StackSize stacksize = defaultStackSize, Time timeSlice = defaultTimeSlice);
	virtual void run();
	
	private:
	static void wrapper(Thread*);
	ID id;
};

void dispatch();

#endif
