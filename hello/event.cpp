#include "event.h"
#include "kernelev.h"
#include "ivtentry.h"
#include "core.h"
#include "booleans.h"

#define ENTER_SYSTEM_MODE(num, par1, par2, par3) System::inSystemMode = true;\
volatile CallParameters* cp = &CallParameters(id, num, par1, par2, par3);\
volatile unsigned mySeg = FP_SEG(cp);\
volatile unsigned myOff = FP_OFF(cp);\
asm {mov ax, mySeg}; \
asm {mov bx, myOff}; \
asm int 60h

Event::Event(IVTNo ivtNo) {
	lock;
	id = Core::allEvs.getID();
	if (System::systemInitialized) {
#ifndef BCC_BLOCK_IGNORE
		ENTER_SYSTEM_MODE(8, this, &ivtNo, NULL);
#endif
	} else
		Core::allEvs.add(new KernelEv(this, ivtNo), id);
	unlock;
}

Event::~Event() {
	lock;
	if (System::systemInitialized) {
#ifndef BCC_BLOCK_IGNORE
		ENTER_SYSTEM_MODE(9, NULL, NULL, NULL);
#endif
	} else if (Core::allEvs.get(id))
		delete Core::allEvs.erase(id);
	unlock;
}

void Event::wait() {
	lock;
	if (System::systemInitialized) {
#ifndef BCC_BLOCK_IGNORE
		ENTER_SYSTEM_MODE(10, NULL, NULL, NULL);
#endif
	} else if (Core::allEvs.get(id)->wait()) {
		unlock;
		Core::dispatch();
	}
	unlock;
}

void Event::signal() {
	lock;
	Core::allEvs.get(id)->signal();
	unlock;
}
