#include "timer.h"
#include <dos.h>
#include "thread.h"
#include "pcb.h"
#include "core.h"
#include "stdlib.h"

extern void tick();

#define MAX_SLEEP_TIME (50)

pInterrupt Timer::oldRoutine = 0;
PCBQueue** Timer::sleepingThreads = NULL;
unsigned int Timer::pointerToElapsed = 0;

Timer::Timer() {
#ifndef BCC_BLOCK_IGNORE
	Timer::oldRoutine = getvect(0x08);
	setvect(0x08, newRoutine);
#endif
	Timer::sleepingThreads = new PCBQueue*[MAX_SLEEP_TIME];
	for (unsigned int i = 0; i < MAX_SLEEP_TIME; i++)
	sleepingThreads[i] = new PCBQueue();
}

Timer::~Timer() {
	lock;
#ifndef BCC_BLOCK_IGNORE
	setvect(0x08, oldRoutine);
#endif
	PCB* helper;
	for (unsigned int i = 0; i < MAX_SLEEP_TIME; i++) {
		if (sleepingThreads[i])
			while (!sleepingThreads[i]->isEmpty()) {
				helper = sleepingThreads[i]->pop_front();
				helper->state = PCB::READY;
				Scheduler::put(helper);
			}
		delete sleepingThreads[i];
	}
	delete [] sleepingThreads;
	unlock;
}

void interrupt Timer::newRoutine(...) {
	tick();
	Timer::oldRoutine(); //to ensure system faultlessness
	Timer::wakeUp();
	if (!Core::running->lockFlag && !KernelEv::blockedEvents.isEmpty())
		KernelEv::unblockNow();
	if (PCB::decAndCheckTime() && !Core::running->lockFlag) {
		Core::running->state = PCB::READY;
		Core::dispatch();
	}
}

void Timer::addToSleepQueue(PCB *p, Time t) {
	lock;
	Time appropriateTime = (t + pointerToElapsed - 1) % MAX_SLEEP_TIME;
	sleepingThreads[t]->push_back(p);
	unlock;
}

void Timer::wakeUp() {
	lock;
	pointerToElapsed = (pointerToElapsed + 1) % MAX_SLEEP_TIME;
	PCBQueue *first = sleepingThreads[0];
	for (int i = 1; i < MAX_SLEEP_TIME; i++)
		sleepingThreads[i-1] = sleepingThreads[i];
	PCBQueue* helper = sleepingThreads[0];
	while (!helper->isEmpty()) {
		PCB* PCBhelper = helper->pop_back();
		PCBhelper->state = PCB::READY;
		Scheduler::put(PCBhelper);
	}
	sleepingThreads[MAX_SLEEP_TIME-1] = first;
	unlock;
}
