#include "idle.h"
#include "pcb.h"
#include "core.h"


Idle::Idle(): Thread(256, 1) {
	start();
} //size, duration (not being 0 ensures the scheduler will be periodically checked for freshly unblocked threads)

void Idle::run() { //endless loop while things are changed
	while (true);
}

void Idle::start() {
	lock;
	Core::allPCBs.get(PCB::IDLE)->state = PCB::READY; //will be statically held inside core
	Core::allPCBs.get(PCB::IDLE)->create_context(); //Core::get because not yet initialized
	unlock;
}
