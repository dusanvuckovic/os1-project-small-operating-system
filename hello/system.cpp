#include "system.h"
#include "thread.h"
#include "pcb.h"
#include "callpar.h"
#include "stdlib.h"

volatile bool System::systemInitialized = false;
volatile bool System::inSystemMode = false;
volatile bool System::swapNeeded = false;
volatile unsigned System::sysSP = 0, System::sysSS = 0;

unsigned System::segments[numOfSysCalls];
unsigned System::offsets[numOfSysCalls];
void (*System::calls[numOfSysCalls]) (CallParameters*);

System::System(unsigned* stack) {

	systemStack = stack;

	//and others
	#ifndef BCC_BLOCK_IGNORE
	segments[THRD_C] = FP_SEG(PCB::c_sys);
	segments[THRD_D] = FP_SEG(PCB::d_sys);
	segments[THRD_WTC] = FP_SEG(PCB::wtc_sys);
	segments[THRD_SL] = FP_SEG(PCB::slp_sys);
	segments[SMPHR_C] = FP_SEG(KernelSem::c_sys);
	segments[SMPHR_D] = FP_SEG(KernelSem::d_sys);
	segments[SMPHR_W] = FP_SEG(KernelSem::w_sys);
	segments[SMPHR_S] = FP_SEG(KernelSem::s_sys);
	segments[EVNT_C] = FP_SEG(KernelEv::c_sys);
	segments[EVNT_D] = FP_SEG(KernelEv::d_sys);
	segments[EVNT_W] = FP_SEG(KernelEv::w_sys);

	offsets[THRD_C] = FP_OFF(PCB::c_sys);
	offsets[THRD_D] = FP_OFF(PCB::d_sys);
	offsets[THRD_WTC] = FP_OFF(PCB::wtc_sys);
	offsets[THRD_SL] = FP_OFF(PCB::slp_sys);
	offsets[SMPHR_C] = FP_OFF(KernelSem::c_sys);
	offsets[SMPHR_D] = FP_OFF(KernelSem::d_sys);
	offsets[SMPHR_W] = FP_OFF(KernelSem::w_sys);
	offsets[SMPHR_S] = FP_OFF(KernelSem::s_sys);
	offsets[EVNT_C] = FP_OFF(KernelEv::c_sys);
	offsets[EVNT_D] = FP_OFF(KernelEv::d_sys);
	offsets[EVNT_W] = FP_OFF(KernelEv::w_sys);

	sysSP = FP_OFF(systemStack + MAX_STACK_SIZE - 1);
	sysSS = FP_SEG(systemStack + MAX_STACK_SIZE - 1);

	setvect(0x60, systemCall);
	#endif
	systemInitialized = true; /*zakomentarisati za ne-sistemski mod*/
}

System::~System() {
	systemInitialized = false;
	if (systemStack)
		delete systemStack;
	#ifndef BCC_BLOCK_IGNORE
	setvect(0x60, NULL);
	#endif
}

void interrupt System::systemCall(...) {
	static volatile unsigned oldSP = 0, oldSS = 0, oldBP = 0;
	static volatile unsigned sysCS = 0, sysIP = 0;
	static volatile unsigned holdOFF, holdSEG;
	static CallParameters* cp;
	#ifndef BCC_BLOCK_IGNORE
	asm{
		mov bx, [bp+14]
		mov ax, [bp+16]
		mov holdSEG, ax
		mov holdOFF, bx
	};
	cp = (CallParameters*) MK_FP(holdSEG, holdOFF);
	#endif

	sysCS = segments[cp->opcode];
	sysIP = offsets[cp->opcode];
	volatile static unsigned newSP = sysSP;
	volatile static unsigned newSS = sysSS;
	#ifndef BCC_BLOCK_IGNORE
	asm{
		mov oldSP, sp
		mov oldSS, ss
		mov oldBP, bp
	};
	#endif
	Core::running->tsp = oldSP;
	Core::running->tss = oldSS;
	Core::running->tbp = oldBP;
	#ifndef BCC_BLOCK_IGNORE
	asm{
		mov sp, newSP
		mov ss, newSS
		mov ax, holdSEG
		push ax
		mov ax, holdOFF
		push ax
		pushf
		pop ax
		or ax, 0x0200 //interrupts enabled
		push ax //psw
		mov ax, sysCS
		push ax //CS
		mov ax, sysIP
		push ax //IP
		mov ax, 0
		push ax //AX
		push bx
		push cx
		push dx
		push es
		push ds
		push si
		push di
		push bp
	};
	#endif
	return;
}
