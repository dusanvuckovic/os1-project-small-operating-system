#ifndef _event_h_
#define _event_h_

#include "ivtentry.h"
#include "core.h"

typedef unsigned char IVTNo;
class Core;
class KernelEv;
class IVTEntry;

class Event {

public:
	Event (IVTNo ivtNo);
	~Event ();
		
	void wait ();
		
protected:
	friend class KernelEv;
	void signal(); // can call KernelEv

private:
	ID id;
};
#endif
