#ifndef _arrayg_h_
#define _arrayg_h_
#include <booleans.h>
#include <assert.h>
#include <stdlib.h>

#pragma warn -w-inl


template<class T>
class ArrayG {

private:
	T* data;
	bool *valid;
	unsigned int totalCapacity, currentCapacity, size;
	unsigned int nextID;

	void resize() {
		unsigned int i;
		unsigned int newCapacity = totalCapacity + totalCapacity / 2;
		T* newData = new T[newCapacity];
		bool *newValid = new bool[newCapacity];
		for (i = 0; i < totalCapacity; i++) {
			newData[i] = data[i];
			newValid[i] = valid[i];
		}
		for (i = totalCapacity; i < newCapacity; i++) {
			newData[i] = 0;
			newValid[i] = false;
		}
		delete[] data;
		delete[] valid;
		data = newData;
		valid = newValid;
		totalCapacity = newCapacity;
	}

public:

	ArrayG(unsigned int capacity): totalCapacity(capacity), size(0), nextID(0), currentCapacity(0) {
		data = new T[capacity];
		valid = new bool[capacity];
		for (unsigned int i = 0; i < capacity; i++) {
			data[i] = 0;
			valid[i] = false;
		}
	}

	~ArrayG() {
		delete[] data;
		delete[] valid;
	}
	void add(T t) {
		if (currentCapacity == totalCapacity)
			resize();
		while (!valid[currentCapacity]) {
			++currentCapacity;
			if (currentCapacity == totalCapacity)
				resize();
		}
		valid[currentCapacity] = true;
		data[currentCapacity++] = t;
		++size;
	}

	void add(T t, ID id) {
		while (totalCapacity <= id)
			resize();
		data[id] = t;
		valid[id] = true;
		++size;
	}

	ID getID() {
		return nextID++;
	}

	T erase(ID id) {
		T helper = get(id);
		valid[id] = false;
		data[id] = 0;
		--size;
		return helper;
	}

	T get(ID id) {
		if (id < 0) {
			printf("Greska pri dohvatanju: zahteva se negativni element %i.\n",
					id);
			return 0;
		} else if (id >= totalCapacity) {
			printf("Greska pri uklanjanju: zahteva se element %, veci od elementa %i.\n", id, totalCapacity);
			return 0;
		}
		if (!valid[id]) {
			printf("Greska pri dohvatanju: zahteva se nevalidan element %i niza!\n", id);
			return 0;
		} else
			return data[id];
	}

	inline unsigned int getSize() const {
		return size;
	}
	inline bool isEmpty() {
		return !size;
	}
};

#endif
