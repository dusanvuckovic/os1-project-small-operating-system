#ifndef _queueg_h
#define _queueg_h
#include "booleans.h"

template<class T>
class QueueG {

private:
	struct QNode {
	public:
		T* t;
		QNode *next, *previous;
		QNode(T* t) {
			this->t = t;
			next = previous = NULL;
		}
		~QNode() {
			t = NULL;
			next = previous = NULL;
		}
	};
	QNode *head, *tail;
	unsigned int numOfElements;

public:
	QueueG() {
		head = tail = NULL;
		numOfElements = 0;
	}

	~QueueG() {
		QNode *iterate = head, *helper = NULL;
		while (iterate) {
			helper = iterate;
			iterate = iterate->next;
			helper->t = NULL;
			delete helper;
		}
		head = tail = NULL;
	}

	bool isEmpty() {
		if (numOfElements)
			return false;
		else
			return true;
	}

	unsigned int size() const {
		return numOfElements;
	}

	void push_back(T* t) {
		QNode *n = new QNode(t);
		if (!tail)
			head = tail = n;
		else {
			tail->next = n;
			n->previous = tail;
			tail = n;
		}
		++numOfElements;
	}

	T* pop_front() {
		if (!head)
			return NULL;
		QNode *n = head;
		T* helper = n->t;
		if (head == tail)
			head = tail = NULL;
		else {
			head = head->next;
			head->previous = NULL;
		}
		delete n;
		--numOfElements;
		return helper;
	}

	T* pop_back() {
		if (!tail)
			return NULL;
		QNode *n = tail;
		T* helper = n->t;
		if (head == tail)
			head = tail = NULL;
		else {
			tail = tail->previous;
			tail->next = NULL;
		}
		delete n;
		--numOfElements;
		return helper;
	}

};
#endif /* QUEUEG_H_ */
